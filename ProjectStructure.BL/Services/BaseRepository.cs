﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Data;
using ProjectStructure.Ef;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Services {
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity {
        // public List<TEntity> _dbSet;
        public DbSet<TEntity> _dbSet;
        public ProjectStructureDbContext _dbContext;
        private IDbInitializer _dbInitializer;

        // public BaseRepository(IDbInitializer dbInitializer) {
        //     _dbInitializer = dbInitializer;
        //     _dbSet = _dbInitializer.Get<TEntity>();
        // }

        public BaseRepository(ProjectStructureDbContext dbContext) {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null) {
            IQueryable<TEntity> query = _dbSet.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return await query.ToListAsync();
        }

        public async Task Create(TEntity entity) {
            await _dbSet.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(TEntity entity) {
            var item = await _dbSet.FirstOrDefaultAsync(i => i.Id == entity.Id);
            if (item != null) {
                _dbSet.Update(entity);
            } else await _dbSet.AddAsync(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(TEntity entity) {
            _dbSet.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}