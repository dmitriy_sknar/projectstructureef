﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Services {
    public class TasksService : ITasksService {
        public IRepository<BsaTask> _repo;

        public TasksService(IRepository<BsaTask> repo) {
            _repo = repo;
        }

        public async Task<IEnumerable<BsaTask>> Get() {
            return await _repo.Get();
        }

        public async Task<IEnumerable<BsaTask>> Get(int id) {
            return await _repo.Get(x => x.Id == id);
        }

        public async Task Create(BsaTask entity) {
            var existingEntity = await _repo.Get(e => e.Id == entity.Id);
            if (existingEntity.Count() != 0) {
                throw new InvalidOperationException($"Cannot create {nameof(entity)}. {nameof(entity)} already exists");
            }

            await _repo.Create(entity);
        }

        public async Task Update(BsaTask entity) {
            await _repo.Update(entity);
        }

        public async Task Delete(BsaTask entity) {
            await _repo.Delete(entity);
        }

        public async Task<IEnumerable<BsaTask>> GetTasksInProgressForUser(int userId) {
            return await _repo.Get(e => e.PerformerId == userId
                                             && e.StateId != (int)TaskStatesEnum.Finished
                                             && e.StateId != (int)TaskStatesEnum.Canceled);
        }
    }
}