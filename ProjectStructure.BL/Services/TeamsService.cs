﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Services
{
    public class TeamsService : ITeamsService {
        public IRepository<Team> _repo;

        public TeamsService(IRepository<Team> repo) {
            _repo = repo;
        }

        public async Task<IEnumerable<Team>> Get() {
            return await _repo.Get();
        }

        public async Task<IEnumerable<Team>> Get(int id) {
            return await _repo.Get(x => x.Id == id);
        }

        public async Task Create(Team entity) {
            var existingEntity = await _repo.Get(e => e.Id == entity.Id);
            if (existingEntity.Count() != 0) {
                throw new InvalidOperationException($"Cannot create {nameof(entity)}. {nameof(entity)} already exists");
            }

            await _repo.Create(entity);
        }

        public async Task Update(Team entity) {
            await _repo.Update(entity);
        }

        public async Task Delete(Team entity) {
            await _repo.Delete(entity);
        }
    }
}
