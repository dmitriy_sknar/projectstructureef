﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Services {
    public class UsersService : IUsersService {
        public IRepository<User> _repo;

        public UsersService(IRepository<User> repo) {
            _repo = repo;
        }

        public async Task<IEnumerable<User>> Get() {
            return await _repo.Get();
        }

        public async Task<IEnumerable<User>> Get(int id) {
            return await _repo.Get(x => x.Id == id);
        }

        public async Task Create(User entity) {
            var existingEntity = await _repo.Get(e => e.Id == entity.Id);
            if (existingEntity.Count() != 0) {
                throw new InvalidOperationException($"Cannot create {nameof(entity)}. {nameof(entity)} already exists");
            }

            await _repo.Create(entity);
        }

        public async Task Update(User entity) {
            await _repo.Update(entity);
        }

        public async Task Delete(User entity) {
            await _repo.Delete(entity);
        }
    }
}