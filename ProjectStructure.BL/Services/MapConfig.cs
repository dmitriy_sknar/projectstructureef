﻿using AutoMapper;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.BL.Services {
    public class MapConfig : Profile {
        public MapConfig() {
            CreateMap<ProjectDto, Project>();
            CreateMap<Project, ProjectDto>();

            CreateMap<BsaTaskDto, BsaTask>();
            CreateMap<BsaTask, BsaTaskDto>();
            
            CreateMap<UserDto, User>();
            CreateMap<User, UserDto>();
            
            CreateMap<TeamDto, Team>();
            CreateMap<Team, TeamDto>();
            
            CreateMap<TaskStateDto, TaskState>();
            CreateMap<TaskState, TaskStateDto>();
        }
    }
}