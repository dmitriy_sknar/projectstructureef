﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Services {
    public class ProjectsService : IProjectsService {
        public IRepository<Project> _repo;

        public ProjectsService(IRepository<Project> repo) {
            _repo = repo;
        }

        public async Task<IEnumerable<Project>> Get() {
            return await _repo.Get();
        }

        public async Task<IEnumerable<Project>> Get(int id) {
            return await _repo.Get(x => x.Id == id);
        }

        public async Task Create(Project entity) {
            var existingEntity = await _repo.Get(e => e.Id == entity.Id);
            if (existingEntity.Count() != 0) {
                throw new InvalidOperationException($"Cannot create {nameof(entity)}. {nameof(entity)} already exists");
            }

            await _repo.Create(entity);
        }

        public async Task Update(Project entity) {
            await _repo.Update(entity);
        }

        public async Task Delete(Project entity) {
            await _repo.Delete(entity);
        }

        public async Task<Dictionary<Project, int>> GetTasksPerUserProject(int userId) {
            var projects = await _repo.Get(p => p.AuthorId == userId);
            Dictionary<Project, int> dict = projects.Select(p => new KeyValuePair<Project, int>(p, p.Tasks.Count))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            return dict;
        }

        public async Task<IEnumerable<BsaTask>> GetTasksPerUserWithNameLengthUpTo45Symbols(int userId) {
            var projects = await _repo.Get(p =>
                p.Tasks.Any(t => t.Name.Length < 45
                                 && t.PerformerId == userId));
            var list = projects
                .SelectMany(p => p.Tasks)
                .ToList();

            return list;
        }

        //ToDo: inject and switch to BsaTask repo
        public async Task<IEnumerable<BsaTask>> GetTasksPerUserFinishedIn2020(int userId) {
            var projects = await _repo.Get();
            return projects
                .SelectMany(p => p.Tasks)
                .Where(t => DateTime.Compare(t.FinishedAt, DateTime.Now) <= 0
                            && t.PerformerId == userId)
                .ToList();
        }

        public async Task<IEnumerable<Team>> GetTeamsWhereAllUsersOlder10Years() {
            var projects = await _repo.Get(proj => proj.Team.TeamMates
                .All(mate => DateTime.Compare(mate.Birthday, DateTime.Today.AddYears(-10)) <= 0));
            return projects
                .Select(p => p.Team)
                .Distinct()
                .ToList();
        }

        public async Task<IEnumerable<User>> GetUsersSortedByFirstNameAndTaskNameLength() {
            var projects = await _repo.Get();
            return projects
                .SelectMany(p => p.Tasks)
                .Select(task => task.Performer)
                .Distinct()
                .OrderBy(user => user.FirstName)
                .GroupJoin(
                    projects.SelectMany(p => p.Tasks),
                    user => user.Id,
                    bsaTask => bsaTask.PerformerId,
                    (user, bsaTaskList) => {
                        bsaTaskList = bsaTaskList.OrderByDescending(task => task.Name.Length);
                        user.Tasks = bsaTaskList.ToList();
                        return user;
                    });
        }

        public async Task<IEnumerable<Project>> GetUserAndProjectsDetailsSortedByProjectCreatedDateDescending(int userId) {
            var projects = await _repo.Get(proj => proj.AuthorId == userId);
            return projects
                .OrderByDescending(proj => proj.CreatedAt).Take(1);
        }

        public async Task<Dictionary<Project, int>> GetProjectWithNameLengthFrom20AndTasksCountTo3WithTeamMatesCount() {
            var projects = await _repo.Get();
            var projectWithDetails = projects.Select(pr => {
                    int teamMatesCount = 0;
                    int tasksCount = pr.Tasks?.Count ?? 0;
                    if (pr.Name.Length > 20 || tasksCount < 3 && tasksCount > 0) {
                        teamMatesCount = pr.Team?.TeamMates?.Count ?? 0;
                    }

                    return new KeyValuePair<Project, int>(pr, teamMatesCount);
                })
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return projectWithDetails;
        }
    }
}