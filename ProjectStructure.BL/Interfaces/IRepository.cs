﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Interfaces {
    public interface IRepository<TEntity> where TEntity : Entity {

        Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null);

        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
    }
}