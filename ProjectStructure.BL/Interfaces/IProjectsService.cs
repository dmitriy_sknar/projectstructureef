﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Interfaces {
    public interface IProjectsService {
        Task<IEnumerable<Project>> Get();

        Task<IEnumerable<Project>> Get(int id);

        Task Create(Project entity);

        Task Update(Project entity);

        Task Delete(Project entity);

        Task<Dictionary<Project, int>> GetTasksPerUserProject(int userId);

        Task<IEnumerable<BsaTask>> GetTasksPerUserWithNameLengthUpTo45Symbols(int userId);

        Task<IEnumerable<BsaTask>> GetTasksPerUserFinishedIn2020(int userId);

        Task<IEnumerable<Team>> GetTeamsWhereAllUsersOlder10Years();

        Task<IEnumerable<User>> GetUsersSortedByFirstNameAndTaskNameLength();

        Task<IEnumerable<Project>> GetUserAndProjectsDetailsSortedByProjectCreatedDateDescending(int userId);

        Task<Dictionary<Project, int>> GetProjectWithNameLengthFrom20AndTasksCountTo3WithTeamMatesCount();

    }
}