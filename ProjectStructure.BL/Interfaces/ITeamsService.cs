﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Interfaces {
    public interface ITeamsService {
        Task<IEnumerable<Team>> Get();

        Task<IEnumerable<Team>> Get(int id);

        Task Create(Team entity);

        Task Update(Team entity);

        Task Delete(Team entity);
    }
}