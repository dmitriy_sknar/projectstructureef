﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Interfaces {
    public interface IUsersService {
        Task<IEnumerable<User>> Get();

        Task<IEnumerable<User>> Get(int id);

        Task Create(User entity);

        Task Update(User entity);

        Task Delete(User entity);
    }
}