﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Interfaces {
    public interface ITasksService {
        Task<IEnumerable<BsaTask>> Get();

        Task<IEnumerable<BsaTask>> Get(int id);

        Task Create(BsaTask entity);

        Task Update(BsaTask entity);

        Task Delete(BsaTask entity);

        Task<IEnumerable<BsaTask>> GetTasksInProgressForUser(int userId);
    }
}