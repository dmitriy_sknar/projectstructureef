﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Data;
using ProjectStructure.Model;

namespace ProjectStructure.Ef {

    public sealed class ProjectStructureDbContext : DbContext {

        private DbInitializer DbDataInitializer = new DbInitializer();
        public ProjectStructureDbContext(DbContextOptions<ProjectStructureDbContext> options) 
        : base(options) {
            // Database.EnsureCreated();
        }

        public DbSet<BsaTask> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<TaskState> TaskStates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<UserAddress>()
                .HasKey(ua => new { ua.UserId, ua.AddressId });
            modelBuilder.Entity<UserAddress>()
                .HasOne(ua => ua.User)
                .WithMany(u => u.UserAddresses)
                .HasForeignKey(ua => ua.AddressId);
            modelBuilder.Entity<UserAddress>()
                .HasOne(ua => ua.Address)
                .WithMany(u => u.UserAddresses)
                .HasForeignKey(ua => ua.UserId);

            modelBuilder.Entity<TaskState>() 
                .Property(p => p.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<BsaTask>().HasData(DbDataInitializer.Tasks);
            modelBuilder.Entity<Project>().HasData(DbDataInitializer.Projects);
            modelBuilder.Entity<TaskState>().HasData(DbDataInitializer.TaskStates);
            modelBuilder.Entity<Team>().HasData(DbDataInitializer.Teams);
            modelBuilder.Entity<User>().HasData(DbDataInitializer.Users);

            base.OnModelCreating(modelBuilder);
        }
    }
}