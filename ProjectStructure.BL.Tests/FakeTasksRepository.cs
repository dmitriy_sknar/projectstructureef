﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Data;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Tests {
    public class FakeTasksRepository : IRepository<BsaTask> {
        private DbInitializer DbInitializer;

        public List<BsaTask> _dbContext;

        public FakeTasksRepository() {
            DbInitializer = new DbInitializer();
            DbInitializer.InitDb();
            _dbContext = DbInitializer.Tasks;
        }

        public async Task<IEnumerable<BsaTask>> Get(Expression<Func<BsaTask, bool>> filter = null) {
            IQueryable<BsaTask> query = _dbContext.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public async Task Create(BsaTask entity) {
            _dbContext.Add(entity);
        }

        public async Task Update(BsaTask entity) {
            var item = _dbContext.FirstOrDefault(i => i.Id == entity.Id);
            if (item != null) item = entity;
            else _dbContext.Add(entity);
        }

        public async Task Delete(BsaTask entity) {
            var existedEntity = _dbContext.FirstOrDefault(e => e.Id == entity.Id);
            if (existedEntity != null) {
                _dbContext.Remove(existedEntity);
            }
        }
    }
}