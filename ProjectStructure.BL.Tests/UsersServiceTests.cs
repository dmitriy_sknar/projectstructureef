﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.Model;
using Xunit;

namespace ProjectStructure.BL.Tests {
    public class UsersServiceTests {

        private IRepository<User> _repo;
        private IUsersService _service;

        public UsersServiceTests() {
            _repo = new FakeUsersRepository();
            _service = new UsersService(_repo);
        }

        [Fact]
        public void CreateUserTest() {

            //Arrange
            string firstName = "Bob";
            string lastName = "Uncle";
            DateTime birthday = DateTime.Parse("1986-07-01T10:04:14.9700000");

            var user = new User() {
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                RegisteredAt = DateTime.Now,
                Email = "Blabla@super.org"
            };

            //Act
            _service.Create(user);
            User createdUser = _service
                .Get().Result
                .FirstOrDefault(u=>u.FirstName == firstName
                && u.LastName == lastName
                && u.Birthday == birthday);

            //Asset
            Assert.NotNull(createdUser);
            Assert.Equal(firstName, createdUser.FirstName);
            Assert.Equal(lastName, createdUser.LastName);
            Assert.Equal(birthday, createdUser.Birthday);
        }

        [Fact]
        public void CreateExistingUserTest() {

            //Arrange
            string firstName = "Bob";
            string lastName = "Uncle";
            DateTime birthday = DateTime.Parse("1986-07-01T10:04:14.9700000");

            var user = new User() {
                Id = 1,
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                RegisteredAt = DateTime.Now,
                Email = "Blabla@super.org"
            };

            //Act and Assert
            Assert.ThrowsAsync<InvalidOperationException>( () => _service.Create(user));
        }
    }
}