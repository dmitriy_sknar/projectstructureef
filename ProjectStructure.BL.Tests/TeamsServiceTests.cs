﻿using System;
using System.Linq;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.BL.Tests.Files;
using ProjectStructure.Model;
using Xunit;

namespace ProjectStructure.BL.Tests {
    public class TeamsServiceTests {
        private IRepository<Team> _repo;
        private ITeamsService _service;

        public TeamsServiceTests() {
            _repo = new FakeTeamsRepository();
            _service = new TeamsService(_repo);
        }

        [Fact]
        public void AddUserToTeamTest() {

            //Arrange
            var team = _repo.Get().Result
                .FirstOrDefault(t => t.TeamMates.Count > 0);
            int teamMatesCountBefore = team.TeamMates.Count;

            //test data in fakeRepo is broken
            Assert.NotNull(team);

            string firstName = "Bob";
            string lastName = "Uncle";
            DateTime birthday = DateTime.Parse("1986-07-01T10:04:14.9700000");

            var user = new User() {
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                RegisteredAt = DateTime.Now,
                Email = "Blabla@super.org",
                Team = team
            };

            team.TeamMates.Add(user);

            //Act
            _service.Update(team);
            Team updatedTeam = _service
                .Get().Result
                .FirstOrDefault(t => t.Id == team.Id);

            //Asset
            Assert.NotNull(updatedTeam);
            Assert.Equal(teamMatesCountBefore + 1, updatedTeam.TeamMates.Count);
            Assert.True(updatedTeam.TeamMates.Any(u => u.Id == user.Id));
        }
    }
}