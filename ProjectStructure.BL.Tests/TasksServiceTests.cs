﻿using System;
using System.Linq;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.Model;
using Xunit;

namespace ProjectStructure.BL.Tests {
    public class TasksServiceTests {
        private IRepository<BsaTask> _repo;
        private ITasksService _service;

        public TasksServiceTests() {
            _repo = new FakeTasksRepository();
            _service = new TasksService(_repo);
        }

        [Fact]
        public void UpdateTaskTest() {
            //Arrange
            var task = _service
                .Get().Result
                .FirstOrDefault(t => t.StateId == (int)TaskStatesEnum.Created);
            var finishedTime = DateTime.Now;

            //Act
            task.StateId = (int)TaskStatesEnum.Finished;
            task.TaskState = new TaskState() {
                Id = (int)TaskStatesEnum.Finished,
                Value = TaskStatesEnum.Finished.ToString()
            };
            task.FinishedAt = finishedTime;

            _service.Update(task);

            var updatedTask = _service.Get(task.Id).Result.First();

            //Asset
            Assert.NotNull(updatedTask);
            Assert.Equal((int)TaskStatesEnum.Finished, updatedTask.StateId);
            Assert.Equal(TaskStatesEnum.Finished.ToString(), updatedTask.TaskState.Value);
            Assert.Equal(finishedTime, updatedTask.FinishedAt);
        }

        [Theory]
        [InlineData(7, 8)]
        [InlineData(40, 1)]
        [InlineData(500, 0)] //no such user
        [InlineData(-1, 0)] //no such user 
        public void GetTasksInProgressForUserTest(int userId, int expectedTasksCount) {
            //Act
            var tasks = _service.GetTasksInProgressForUser(userId).Result;

            //Asset
            Assert.Equal(expectedTasksCount, tasks.ToList().Count);
            Assert.All(tasks, el => Assert.False((int)TaskStatesEnum.Finished == el.PerformerId));
            Assert.All(tasks, el => Assert.False((int)TaskStatesEnum.Canceled == el.PerformerId));
        }
    }
}