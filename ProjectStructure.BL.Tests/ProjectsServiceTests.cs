using System;
using System.Linq;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.Model;
using Xunit;

namespace ProjectStructure.BL.Tests {
    public class ProjectsServiceTests {
        private IRepository<Project> _repo;
        private IProjectsService _service;

        public ProjectsServiceTests() {
            _repo = new FakeProjectsRepository();
            _service = new ProjectsService(_repo);
        }

        [Fact]
        public void GetWithoutFilter_ReturnsAllRecords() {
            //Arrange

            //Act
            int count = _service.Get().Result.Count();

            //Asset
            Assert.Equal(100, count);
        }

        [Fact]
        public void GetTasksPerUserProjectSpecifiedTest() {
            //Arrange
            int userProjId = 1;

            //Act
            var userProjectsWithTasksCount = _service.GetTasksPerUserProject(userProjId).Result;

            //Asset
            Assert.Equal(2, userProjectsWithTasksCount.Keys.Count);
            Assert.Equal(1, userProjectsWithTasksCount.ElementAt(0).Value);
            Assert.Equal(0, userProjectsWithTasksCount.ElementAt(1).Value);
        }

        [Theory]
        [InlineData(1, 7)]
        [InlineData(2, 2)]
        [InlineData(3, 7)]
        [InlineData(4, 4)]
        public void GetTasksPerUserWithNameLengthUpTo45SymbolsTest(int userId, int taskCount) {
            //Arrange

            //Act
            var tasks = _service.GetTasksPerUserWithNameLengthUpTo45Symbols(userId).Result;

            //Asset
            Assert.Equal(taskCount, tasks.Count());
        }

        [Theory]
        [InlineData(1, 0)]
        [InlineData(2, 1)]
        [InlineData(3, 0)]
        [InlineData(4, 0)]
        public void GetTasksPerUserFinishedIn2020Test(int userId, int taskCount) {
            //Arrange

            //Act
            var tasks = _service.GetTasksPerUserFinishedIn2020(userId).Result;

            //Asset
            Assert.Equal(taskCount, tasks.Count());
        }

        [Fact]
        public void GetCommandsWithUsersOlder10YearsTest() {
            //Arrange
            int expectedTeamsCount = 57;

            //Act
            var teams = _service.GetTeamsWhereAllUsersOlder10Years().Result;

            //Asset
            Assert.Equal(expectedTeamsCount, teams.Count());
        }

        [Fact]
        public void GetUsersSortedByFirstNameAndTaskNameLengthTest() {
            //Arrange
            string expectedFirstUserName = "Abe";
            string expectedLastUserName = "Zane";
            int expectedUsersCount = 200;

            //Act
            var users = _service.GetUsersSortedByFirstNameAndTaskNameLength().Result;

            //Asset
            Assert.Equal(expectedFirstUserName, users.ToList().ElementAt(0).FirstName);
            Assert.Equal(expectedLastUserName, users.ToList().ElementAt(expectedUsersCount - 1).FirstName);
            Assert.Equal(expectedUsersCount, users.Count());
        }

        [Theory]
        [InlineData("2020-07-01T10:04:14.9700000", 1)]
        [InlineData("2020-07-01T12:48:54.3590000", 12)]
        [InlineData("2020-07-01T13:55:23.6450000", 17)]
        public void GetUserAndProjectsDetailsSortedByProjectCreatedDateDescendingTest(string date, int userId) {
            //Arrange

            DateTime dt = DateTime.Parse(date);
            int expectedProjectsCount = 1;

            //Act
            var projects = _service.GetUserAndProjectsDetailsSortedByProjectCreatedDateDescending(userId).Result;

            //Asset
            Assert.Equal(dt, projects.ToList().ElementAt(0).CreatedAt);
            Assert.Equal(expectedProjectsCount, projects.Count());
        }

        [Fact]
        public void GetProjectWithNameLengthFrom20AndTasksCountTo3WithTeamMatesCountTest() {
            //Arrange
            string expectedFirstProjectName = "Expedita amet quas id a.";
            string expectedLastProjectName = "Quam veniam distinctio ut magnam.";
            int expectedProjectsCount = 100;
            int expectedFirstProjectTeamMatesCount = 5;
            int expectedLastProjectTeamMatesCount = 5;

            //Act
            var dict = _service.GetProjectWithNameLengthFrom20AndTasksCountTo3WithTeamMatesCount().Result;

            //Asset
            Assert.Equal(expectedProjectsCount, dict.Keys.Count);
            Assert.Equal(expectedFirstProjectName, dict.ElementAt(0).Key.Name);
            Assert.Equal(expectedFirstProjectTeamMatesCount, dict.ElementAt(0).Value);
            Assert.Equal(expectedLastProjectName, dict.ElementAt(expectedProjectsCount - 1).Key.Name);
            Assert.Equal(expectedLastProjectTeamMatesCount, dict.ElementAt(expectedProjectsCount - 1).Value);
        }
    }
}