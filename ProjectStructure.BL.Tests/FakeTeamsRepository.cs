﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Data;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Tests.Files
{
    public class FakeTeamsRepository : IRepository<Team> {
        private DbInitializer DbInitializer;

        public List<Team> _dbContext;

        public FakeTeamsRepository() {
            DbInitializer = new DbInitializer();
            DbInitializer.InitDb();
            _dbContext = DbInitializer.Teams;
        }

        public async Task<IEnumerable<Team>> Get(Expression<Func<Team, bool>> filter = null) {
            IQueryable<Team> query = _dbContext.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public async Task Create(Team entity) {
            _dbContext.Add(entity);
        }

        public async Task Update(Team entity) {
            var item = _dbContext.FirstOrDefault(i => i.Id == entity.Id);
            if (item != null) {
                _dbContext.Remove(item);
                _dbContext.Add(entity);
            }
            else _dbContext.Add(entity);
        }

        public async Task Delete(Team entity) {
            var existedEntity = _dbContext.FirstOrDefault(e => e.Id == entity.Id);
            if (existedEntity != null) {
                _dbContext.Remove(existedEntity);
            }
        }
    }
}
