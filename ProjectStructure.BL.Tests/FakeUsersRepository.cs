﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Data;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Tests {
    public class FakeUsersRepository : IRepository<User> {
        private DbInitializer DbInitializer;

        public List<User> _dbContext;

        public FakeUsersRepository() {
            DbInitializer = new DbInitializer();
            DbInitializer.InitDb();
            _dbContext = DbInitializer.Users;
        }

        public async Task<IEnumerable<User>> Get(Expression<Func<User, bool>> filter = null) {
            IQueryable<User> query = _dbContext.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public async Task Create(User entity) {
            _dbContext.Add(entity);
        }

        public async Task Update(User entity) {
            var item = _dbContext.FirstOrDefault(i => i.Id == entity.Id);
            if (item != null) item = entity;
            else _dbContext.Add(entity);
        }

        public async Task Delete(User entity) {
            var existedEntity = _dbContext.FirstOrDefault(e => e.Id == entity.Id);
            if (existedEntity != null) {
                _dbContext.Remove(existedEntity);
            }
        }
    }
}