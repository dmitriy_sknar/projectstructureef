﻿using ProjectStructure.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.Data;
using ProjectStructure.Model;

namespace ProjectStructure.BL.Tests {
    public class FakeProjectsRepository : IRepository<Project> {
        private DbInitializer DbInitializer;

        public List<Project> _dbContext;

        public FakeProjectsRepository() {
            DbInitializer = new DbInitializer();
            DbInitializer.InitDb();
            _dbContext = DbInitializer.ProjectDB;
        }

        public async Task<IEnumerable<Project>> Get(Expression<Func<Project, bool>> filter = null) {
            IQueryable<Project> query = _dbContext.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public async Task Create(Project entity) {
            _dbContext.Add(entity);
        }

        public async Task Update(Project entity) {
            var item = _dbContext.FirstOrDefault(i => i.Id == entity.Id);
            if (item != null) item = entity;
            else _dbContext.Add(entity);
        }

        public async Task Delete(Project entity) {
            var existedEntity = _dbContext.FirstOrDefault(e => e.Id == entity.Id);
            if (existedEntity != null) {
                _dbContext.Remove(existedEntity);
            }
        }
    }
}