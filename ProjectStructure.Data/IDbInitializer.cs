﻿using System.Collections.Generic;
using ProjectStructure.Model;

namespace ProjectStructure.Data {
    public interface IDbInitializer {
        void InitDb();

        List<Project> ProjectDB { get; }

        List<TEntity> Get<TEntity>() where TEntity : Entity;
    }
}