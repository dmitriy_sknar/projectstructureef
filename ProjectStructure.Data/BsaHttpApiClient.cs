﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Model.DTO;

namespace ProjectStructure.Data {
    //ToDo make generic httpClient

    public static class BsaHttpApiClient {
        private const string HOST = "http://localhost:8266";
        private const string PROJECTS_URI = HOST + "/api/projects";
        private const string TASKS_URI = HOST + "/api/tasks";
        private const string TASKSTATES_URI = HOST + "/api/taskstates";
        private const string TEAMS_URI = HOST + "/api/teams";
        private const string USERS_URI = HOST + "/api/users";

        public static async Task<string> GetProjectsAsync() {
            return await GetHttpData(PROJECTS_URI);
        }

        public static async Task<string> GetTasksAsync() {
            return await GetTasksAsync(CancellationToken.None);
        }

        public static async Task<string> GetTasksAsync(CancellationToken cancellationToken) {
            return await GetHttpData(TASKS_URI, cancellationToken);
        }

        public static async Task<string> UpdateTasksAsync(int id, string payload) {
            return await UpdateTasksAsync(id, payload, CancellationToken.None);
        }

        public static async Task<string> UpdateTasksAsync(int id, string payload, CancellationToken cancellationToken) {
            return await PutHttpData(TASKS_URI + $"/{id}", payload, cancellationToken);
        }

        public static async Task<string> GetTaskAsync(int id) {
            return await GetTaskAsync(id, CancellationToken.None);
        }

        public static async Task<string> GetTaskAsync(int id, CancellationToken cancellationToken) {
            return await GetHttpData(TASKS_URI + $"/{id}", cancellationToken);
        }

        public static async Task<string> GetTaskStatesAsync() {
            return await GetHttpData(TASKSTATES_URI);
        }

        public static async Task<string> GetTeamsAsync() {
            return await GetHttpData(TEAMS_URI);
        }

        public static async Task<string> GetUsersAsync() {
            return await GetHttpData(USERS_URI);
        }

        public static async Task<string> GetProjectAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/{id}");
        }

        public static async Task<string> GetTasksPerUserProjectDtoAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/TasksPerUserProject/{id}");
        }

        public static async Task<string> GetTasksPerUserWithNameLengthUpTo45SymbolsAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/TasksPerUserWithNameLengthUpTo45Symbols/{id}");
        }

        public static async Task<string> GetTasksPerUserFinishedIn2020(int id) {
            return await GetHttpData(PROJECTS_URI + $"/TasksPerUserFinishedIn2020/{id}");
        }

        public static async Task<string> GetTeamsWithUsersOlder10Years() {
            return await GetHttpData(PROJECTS_URI + "/CommandsWithUsersOlder10Years");
        }

        public static async Task<string> GetUsersSortedByFirstNameAndTaskNameLength() {
            return await GetHttpData(PROJECTS_URI + "/UsersSortedByFirstNameAndTaskNameLength");
        }

        public static async Task<string> GetUserAndProjectsDetails(int id) {
            return await GetHttpData(PROJECTS_URI + $"/UserAndProjectsDetails/{id}");
        }
        public static async Task<string> GetProjectsWithDetails() {
            return await GetHttpData(PROJECTS_URI + "/ProjectWithDetails");
        }

        private static async Task<string> GetHttpData(string uri) {
            return await GetHttpData(uri, CancellationToken.None);
        }

        private static async Task<string> GetHttpData(string uri, CancellationToken token) {
            using var client = new HttpClient();
            HttpResponseMessage response = (await client.GetAsync(uri, token)).EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        private static async Task<string> PutHttpData(string uri, string jsonData, CancellationToken token) {
            using var client = new HttpClient();
            var httpContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
            HttpResponseMessage response = (await client.PutAsync(uri, httpContent, token)).EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}