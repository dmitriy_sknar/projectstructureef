﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model.DTO {
    public class UserDto {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(FirstName)}: {FirstName}, {nameof(LastName)}: {LastName}, {nameof(Email)}: {Email}, {nameof(Birthday)}: {Birthday}, {nameof(RegisteredAt)}: {RegisteredAt}, {nameof(TeamId)}: {TeamId}";
        }
    }
}