﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.Model.DTO {
    public class ProjectWithDetailsDto {
        public ProjectWithDetailsDto(ProjectDto projectDto, BsaTaskDto longestTaskDto, BsaTaskDto shortestTaskDto, int teamMatesCount) {
            ProjectDto = projectDto;
            LongestTaskDto = longestTaskDto;
            ShortestTaskDto = shortestTaskDto;
            TeamMatesCount = teamMatesCount;
        }

        public ProjectDto ProjectDto { get; set; }
        public BsaTaskDto LongestTaskDto { get; set; }
        public BsaTaskDto ShortestTaskDto { get; set; }
        public int TeamMatesCount { get; set; }

        public override string ToString() {
            return $"{nameof(ProjectDto)}: {ProjectDto}, {nameof(LongestTaskDto)}: {LongestTaskDto}, {nameof(ShortestTaskDto)}: {ShortestTaskDto}, {nameof(TeamMatesCount)}: {TeamMatesCount}";
        }
    }
}