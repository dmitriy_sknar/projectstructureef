﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;

namespace ProjectStructure.WebApi.DTO {
    public class ProjectDto {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DeadLine { get; set; }

        public int AuthorId { get; set; }
        public UserDto Author { get; set; }

        public int? TeamId { get; set; }
        public TeamDto Team { get; set; }

        public List<BsaTaskDto> Tasks;

        public override string ToString() {
            return $"{nameof(Tasks)}: {Tasks}, {nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Description)}: {Description}, {nameof(CreatedAt)}: {CreatedAt}, {nameof(DeadLine)}: {DeadLine}, {nameof(AuthorId)}: {AuthorId}, {nameof(Author)}: {Author}, {nameof(TeamId)}: {TeamId}, {nameof(Team)}: {Team}";
        }
    }


}