﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Model {
    public class TaskState : Entity {
        //schema 
        // "id": 0,
        // "value": "string"
        // [Key]
        // [DatabaseGenerated(DatabaseGeneratedOption.None)]
        // public new int Id { get; set; }
        public string Value { get; set; }
    }
}