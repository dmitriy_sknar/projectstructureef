﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model {
    public class Address : Entity {
        public string Town { get; set; }
        public string Street { get; set; }

        public int HouseNumber { get; set; }

        // public int UserId { get; set; }
        // public User User { get; set; }
        public List<UserAddress> UserAddresses { get; set; }
    }
}