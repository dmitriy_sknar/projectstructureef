﻿namespace ProjectStructure.Model {
    public class UserAddress {
        public int UserId { get; set; }
        public User User { set; get; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
    }
}