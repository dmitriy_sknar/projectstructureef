﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model {
    public enum TaskStatesEnum {
        Created = 0,
        Started = 1,
        Finished = 2,
        Canceled = 3
    }
}