﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Model {
    public class User : Entity {
        //schema
        // "id": 0,
        // "firstName": "string",
        // "lastName": "string",
        // "email": "string",
        // "birthday": "2020-07-06T13:23:49.629Z",
        // "registeredAt": "2020-07-06T13:23:49.629Z",
        // "teamId": 0
        // [Required]
        // [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public DateTime Birthday { get; set; }

        [Required]
        public DateTime RegisteredAt { get; set; }

        // public Address Address { get; set; }

        public List<UserAddress> UserAddresses { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public List<BsaTask> Tasks { get; set; }
    }
}