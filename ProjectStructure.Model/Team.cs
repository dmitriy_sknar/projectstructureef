﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.Model {
    public class Team : Entity {
        //schema:
        // "id": 0,
        // "name": "string",
        // "createdAt": "2020-07-06T13:20:04.634Z"

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }

        public List<User> TeamMates { get; set; }
    }
}