﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.DAL;
using ProjectStructure.Data;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.ConsoleUI {
    public class Client {
        private Dictionary<UiActions, string> ActionsDictionary;
        private Queries _queries;

        public Queries Queries => _queries ??= new Queries();

        public void Start() {
            InitActions();
            GreatUser();
            Execute();
        }

        private void Execute() {
            ShowActions();
            UiActions selectedAction = PromptUser();
            ExecuteAction(selectedAction);
            AskToContinue();
        }

        private void AskToContinue() {
            Console.WriteLine("\nDo you want to continue: yes (Y) \\ no (N)?");
            var key = Console.ReadKey().KeyChar;
            if (key == 'y' || key == 'Y') {
                Console.WriteLine();
                Execute();
            } else if (key == 'n' || key == 'N') {
                return;
            } else {
                Console.WriteLine("Please select right choice!");
                AskToContinue();
            }
        }

        private void ShowActions() {
            Console.WriteLine("\nQuery master actions:");
            for (int i = 0; i < ActionsDictionary.Count; i++) {
                Console.WriteLine($"{i + 1} - {ActionsDictionary.ElementAt(i).Value}");
            }
        }

        private void GreatUser() {
            Console.WriteLine("Query master greats you!");
        }

        private UiActions PromptUser() {
            Console.WriteLine("\nExecute an action:");
            var input = Console.ReadLine();
            if (Int32.TryParse(input, out int actionId) && actionId > 0 && actionId <= ActionsDictionary.Count)
                return ActionsDictionary.ElementAt(actionId - 1).Key;
            else {
                Console.WriteLine("Please enter valid action.");
                PromptUser();
            }

            return UiActions.NoAction;
        }

        private void InitActions() {
            ActionsDictionary = new Dictionary<UiActions, string> {
                { UiActions.LoadProjects, "Load all projects" },
                { UiActions.LoadProject, "Load project with id provided." },
                { UiActions.TasksPerUserProject, "Load tasks for user project by User id provided." },
                { UiActions.TasksPerUserWithNameLengthUpTo45Symbols, "Load tasks with name up to 45 symbols for user project by User id provided." },
                { UiActions.TasksPerUserFinishedIn2020, "Load tasks finished in 2020 for user" },
                { UiActions.TeamsWithUsersOlder10Years, "Load teams where all users are older 10 years" },
                { UiActions.UsersSortedByFirstNameAndTaskNameLength, "Load users sorted by first name and task name length" },
                { UiActions.UserAndProjectsDetails, "Load user and projects details" },
                { UiActions.ProjectsWithDetails, "Load projects with details" },
                { UiActions.StartBackgroundJobMarkRandomTaskCompleted, "Start background job - Mark random task as completed." },
                { UiActions.StopBackgroundJobMarkRandomTaskCompleted, "Stop background job - Mark random task as completed." },
            };
        }

        private async Task ExecuteAction(UiActions selectedAction) {
            string input;
            int id;

            switch (selectedAction) {
                case UiActions.LoadProjects:
                    List<ProjectDto> projects;
                    projects = await Queries.GetProjects();

                    Console.WriteLine(String.Join(", ", projects));
                    break;

                case UiActions.LoadProject:
                    Console.WriteLine("Please enter project id (number):");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("Project id is not valid. Please reenter");
                        break;
                    }

                    ProjectDto project;
                    project = await Queries.GetProject(id);

                    Console.WriteLine(String.Join(", ", project));
                    break;

                case UiActions.TasksPerUserProject:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("Project id is not valid. Please reenter");
                        break;
                    }

                    List<TasksPerUserProjectDto> tasksPerUserProject;
                    tasksPerUserProject = await Queries.GetTasksPerUserProject(id);

                    Console.WriteLine(String.Join(", ", tasksPerUserProject));
                    break;

                case UiActions.TasksPerUserWithNameLengthUpTo45Symbols:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("User id is not valid. Please reenter");
                        break;
                    }

                    List<BsaTaskDto> tasksPerUserProjectWithNameUpTo45Symbols;
                    tasksPerUserProjectWithNameUpTo45Symbols = await Queries.GetTasksPerUserWithNameLengthUpTo45Symbols(id);

                    Console.WriteLine(String.Join(", ", tasksPerUserProjectWithNameUpTo45Symbols));
                    break;

                case UiActions.TasksPerUserFinishedIn2020:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("User id is not valid. Please reenter");
                        break;
                    }

                    List<BsaTaskDto> tasksPerUserFinishedIn2020;
                    tasksPerUserFinishedIn2020 = await Queries.GetTasksPerUserFinishedIn2020(id);

                    Console.WriteLine(String.Join(", ", tasksPerUserFinishedIn2020));
                    break;                
                
                case UiActions.TeamsWithUsersOlder10Years:
                    Console.WriteLine("Loading:");
                    
                    List<BsaTaskDto> teamsWithUsersOlder10Years;
                    teamsWithUsersOlder10Years = await Queries.GetTeamsWithUsersOlder10Years();

                    Console.WriteLine(String.Join(", ", teamsWithUsersOlder10Years));
                    break;       
                
                case UiActions.UsersSortedByFirstNameAndTaskNameLength:
                    Console.WriteLine("Loading:");
                    
                    List<BsaTaskDto> usersSortedByFirstNameAndTaskNameLength;
                    usersSortedByFirstNameAndTaskNameLength = await Queries.GetUsersSortedByFirstNameAndTaskNameLength();

                    Console.WriteLine(String.Join(", ", usersSortedByFirstNameAndTaskNameLength));
                    break;                
                
                case UiActions.UserAndProjectsDetails:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("User id is not valid. Please reenter");
                        break;
                    }
                    Console.WriteLine("Loading:");
                    
                    List<BsaTaskDto> userAndProjectsDetails;
                    userAndProjectsDetails = await Queries.GetUserAndProjectsDetails(id);

                    Console.WriteLine(String.Join(", ", userAndProjectsDetails));
                    break;      
                
                case UiActions.ProjectsWithDetails:
                    Console.WriteLine("Loading:");
                    
                    List<BsaTaskDto> projectsWithDetails;
                    projectsWithDetails = await Queries.GetProjectsWithDetails();

                    Console.WriteLine(String.Join(", ", projectsWithDetails));
                    break;

                case UiActions.StartBackgroundJobMarkRandomTaskCompleted:
                    Console.WriteLine("Background task started");
                    await Queries.MarkRandomTaskWithDelay(1000);
                    break;

                case UiActions.StopBackgroundJobMarkRandomTaskCompleted:
                    Console.WriteLine("Background tasks requested to be stopped");
                    Queries.CancelAllActiveQueries();
                    break;

                default:
                    Console.WriteLine("Such action is not supported");
                    break;
            }
        }
    }
}