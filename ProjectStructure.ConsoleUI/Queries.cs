﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Data;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.ConsoleUI {
    public class Queries : IDisposable {
        private int _delay;
        private Random _random;
        private CancellationTokenSource _cts;

        public Queries() {
            _random = new Random();
            _cts = new CancellationTokenSource();
        }

        public async Task MarkRandomTaskWithDelay(int delay) {
            _delay = delay;
            var cancellationToken = _cts.Token;
            while (!cancellationToken.IsCancellationRequested) {
                int id = await MarkRandomTaskCompletedAsync(cancellationToken);
                Console.WriteLine($"Completed task id: {id}");
            }
        }

        public async Task<int> MarkRandomTaskCompletedAsync(CancellationToken cancellationToken) {
            var tcs = new TaskCompletionSource<int>();
            await Task.Run(async () => {
                try {
                    await Task.Delay(_delay);
                    // загружать список задач
                    var tasks = await GetTasks(cancellationToken);

                    // выбирать случайным образом одну из задач
                    int tasksCount = tasks.Count;
                    int randomTaskId = _random.Next(1, tasksCount);
                    var randomTask = await GetTask(randomTaskId, cancellationToken);
                    if (randomTask == null) throw new InvalidDataException("Get random task failed");

                    // слать запрос на сервер - пометить задачу как выполненную
                    randomTask.StateId = (int)TaskStatesEnum.Finished;

                    var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(randomTask));
                    await UpdateTask(randomTaskId, stringPayload, cancellationToken);
                    randomTask = await GetTask(randomTaskId, cancellationToken);

                    if (randomTask == null
                        || randomTask.StateId != (int)TaskStatesEnum.Finished)
                        throw new InvalidDataException("Setting state finished to a random task failed");

                    // если операция прошла успешна, вернуть id помеченного таска, иначе бросить исключение
                    tcs.SetResult(randomTaskId);
                } catch (Exception ex) {
                    tcs.SetException(ex);
                } 
            }, cancellationToken);

            return await tcs.Task;
        }

        public void CancelAllActiveQueries() {
            _cts.Cancel();
        }

        public async Task<List<BsaTaskDto>> GetTasksPerUserWithNameLengthUpTo45Symbols(int id) {
            var json = await BsaHttpApiClient.GetTasksPerUserWithNameLengthUpTo45SymbolsAsync(id);
            var tasksPerUserProject = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasksPerUserProject;
        }
        public async Task<List<BsaTaskDto>> GetTasksPerUserFinishedIn2020(int id) {
            var json = await BsaHttpApiClient.GetTasksPerUserFinishedIn2020(id);
            var tasksPerUserFinishedIn2020 = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasksPerUserFinishedIn2020;
        }        
        
        public async Task<List<BsaTaskDto>> GetTeamsWithUsersOlder10Years() {
            var json = await BsaHttpApiClient.GetTeamsWithUsersOlder10Years();
            var teamsWithUsersOlder10Years = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return teamsWithUsersOlder10Years;
        }        
        
        public async Task<List<BsaTaskDto>> GetUsersSortedByFirstNameAndTaskNameLength() {
            var json = await BsaHttpApiClient.GetUsersSortedByFirstNameAndTaskNameLength();
            var users = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return users;
        }        

        public async Task<List<BsaTaskDto>> GetUserAndProjectsDetails(int id) {
            var json = await BsaHttpApiClient.GetUserAndProjectsDetails(id);
            var users = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return users;
        }        
        
        public async Task<List<BsaTaskDto>> GetProjectsWithDetails() {
            var json = await BsaHttpApiClient.GetProjectsWithDetails();
            var users = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return users;
        }

        public async Task<List<TasksPerUserProjectDto>> GetTasksPerUserProject(int id) {
            var json = await BsaHttpApiClient.GetTasksPerUserProjectDtoAsync(id);
            var tasksPerUserProject = JsonConvert.DeserializeObject<List<TasksPerUserProjectDto>>(json);
            return tasksPerUserProject;
        }

        public async Task<List<TaskStateDto>> GetTaskStates() {
            var json = await BsaHttpApiClient.GetTaskStatesAsync();
            List<TaskStateDto> tastStates = JsonConvert.DeserializeObject<List<TaskStateDto>>(json);
            return tastStates;
        }

        public async Task<List<TeamDto>> GetTeams() {
            var json = await BsaHttpApiClient.GetTeamsAsync();
            List<TeamDto> teams = JsonConvert.DeserializeObject<List<TeamDto>>(json);
            return teams;
        }

        public async Task<List<UserDto>> GetUsers() {
            var json = await BsaHttpApiClient.GetUsersAsync();
            List<UserDto> users = JsonConvert.DeserializeObject<List<UserDto>>(json);
            return users;
        }

        public async Task<List<ProjectDto>> GetProjects() {
            var json = await BsaHttpApiClient.GetProjectsAsync();
            List<ProjectDto> projects = JsonConvert.DeserializeObject<List<ProjectDto>>(json);
            return projects;
        }

        public async Task<ProjectDto> GetProject(int id) {
            var json = await BsaHttpApiClient.GetProjectAsync(id);
            ProjectDto projects = JsonConvert.DeserializeObject<ProjectDto>(json);
            return projects;
        }

        public async Task<List<BsaTaskDto>> GetTasks() {
            return await GetTasks(CancellationToken.None);
        }

        public async Task<List<BsaTaskDto>> GetTasks(CancellationToken cancellationToken) {
            var json = await BsaHttpApiClient.GetTasksAsync(cancellationToken);
            List<BsaTaskDto> tasks = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasks;
        }

        public async Task<BsaTaskDto> GetTask(int id) {
            return await GetTask(id, CancellationToken.None);
        }

        public async Task<BsaTaskDto> GetTask(int id, CancellationToken cancellationToken) {
            var json = await BsaHttpApiClient.GetTaskAsync(id, cancellationToken);
            BsaTaskDto task = JsonConvert.DeserializeObject<BsaTaskDto>(json);
            return task;
        }

        public async Task<List<BsaTaskDto>> UpdateTask(int id, string payload) {
            return await UpdateTask(id, payload, CancellationToken.None);
        }

        public async Task<List<BsaTaskDto>> UpdateTask(int id, string payload, CancellationToken cancellationToken) {
            var json = await BsaHttpApiClient.UpdateTasksAsync(id, payload, cancellationToken);
            List<BsaTaskDto> tasks = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasks;
        }

        public void Dispose() {
            _cts?.Dispose();
        }
    }
}