﻿namespace ProjectStructure.DAL {
    public enum UiActions {
        NoAction = 0,
        LoadProjects = 1,
        LoadProject = 2,
        TasksPerUserProject = 3,
        TasksPerUserWithNameLengthUpTo45Symbols = 4,
        TasksPerUserFinishedIn2020 = 5,
        TeamsWithUsersOlder10Years = 6,
        UsersSortedByFirstNameAndTaskNameLength = 7,
        UserAndProjectsDetails = 8,
        ProjectsWithDetails = 9,
        StartBackgroundJobMarkRandomTaskCompleted = 101,
        StopBackgroundJobMarkRandomTaskCompleted = 102
    }
}