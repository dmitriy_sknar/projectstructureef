﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;
using Xunit;

namespace ProjectStructure.WebApi.IntegrationTests {
    public class TeamsControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable {
        private readonly HttpClient _httpClient;
        private const string TEAMS_URI = "/api/teams";

        public TeamsControllerTests(CustomWebApplicationFactory<Startup> factory) {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task CreateTeamTest() {
            var createdTime = DateTime.Now;
            var team = new TeamDto() {
                Name = "TestName",
                CreatedAt = createdTime,
                TeamMates = new List<UserDto>() {
                    new UserDto() {
                        Id = 1
                    }
                }
            };

            var jsonString = JsonConvert.SerializeObject(team);
            var postHttpResponse = await _httpClient.PostAsync(TEAMS_URI,
                new StringContent(jsonString, Encoding.UTF8, "application/json"));
            postHttpResponse.EnsureSuccessStatusCode();

            var httpResponse = await _httpClient.GetAsync(TEAMS_URI);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            List<TeamDto> teams = JsonConvert.DeserializeObject<List<TeamDto>>(stringResponse);

            var createdTeam = teams.FirstOrDefault(p => p.Name == team.Name);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotNull(createdTeam);
            Assert.Equal(team.CreatedAt, createdTeam.CreatedAt);
            Assert.Equal(team.TeamMates.Count, createdTeam.TeamMates.Count);
        }

        [Fact]
        public async Task AddNewUserToTeam() {

            int teamId = 10;

            string firstName = "Bob";
            string lastName = "Uncle";
            DateTime birthday = DateTime.Parse("1986-07-01T10:04:14.9700000");

            var user = new UserDto() {
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                RegisteredAt = DateTime.Now,
                Email = "Blabla@super.org"
            };

            var httpResponse = await _httpClient.GetAsync(TEAMS_URI + $"/{teamId}");
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            TeamDto team = JsonConvert.DeserializeObject<TeamDto>(stringResponse);

            team.TeamMates.Add(user);

            var jsonString = JsonConvert.SerializeObject(team);
            var postHttpResponse = await _httpClient.PutAsync(TEAMS_URI + $"/{teamId}",
                new StringContent(jsonString, Encoding.UTF8, "application/json"));
            postHttpResponse.EnsureSuccessStatusCode();

            httpResponse = await _httpClient.GetAsync(TEAMS_URI + $"/{teamId}");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            TeamDto updatedTeam = JsonConvert.DeserializeObject<TeamDto>(stringResponse);
            
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotNull(updatedTeam);
            Assert.NotNull(updatedTeam.TeamMates
                .FirstOrDefault(u => u.FirstName == user.FirstName
                && u.LastName == user.LastName
                && u.Birthday == user.Birthday));
        }


        public void Dispose() {
            _httpClient?.Dispose();
        }
    }
}
