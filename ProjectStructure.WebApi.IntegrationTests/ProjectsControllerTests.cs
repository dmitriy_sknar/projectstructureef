using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.Controllers;
using ProjectStructure.WebApi.DTO;
using ProjectStructure.WebApi.IntegrationTests;
using Xunit;

namespace ProjectStructure.WebApi.Tests {
    public class ProjectsControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable {

        private readonly HttpClient _httpClient;
        private const string PROJECTS_URI = "api/projects";

        public ProjectsControllerTests(CustomWebApplicationFactory<Startup> factory) {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task GetProjectsTest() {
            var httpResponse = await _httpClient.GetAsync(PROJECTS_URI);
            httpResponse.EnsureSuccessStatusCode();

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var projects = JsonConvert.DeserializeObject<List<ProjectDto>>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(100, projects.Count);
        }

        [Fact]
        public async Task CreateProjectTest() {

            var deadLineTime = DateTime.Now.AddYears(1);
            var createdTime = DateTime.Now;
            var project = new ProjectDto() {
                Name = "TestName",
                AuthorId = 1,
                DeadLine = deadLineTime,
                CreatedAt = createdTime,
                Description = "TestsDescription",
                TeamId = 1,
                Team = new TeamDto(){ Id = 1 },
                Author = new UserDto(){ Id = 1}
            };

            var jsonString = JsonConvert.SerializeObject(project);
            var postHttpResponse = await _httpClient.PostAsync(PROJECTS_URI,
                new StringContent(jsonString, Encoding.UTF8, "application/json"));
            postHttpResponse.EnsureSuccessStatusCode();

            var httpResponse = await _httpClient.GetAsync(PROJECTS_URI);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            List<ProjectDto> projects = JsonConvert.DeserializeObject<List<ProjectDto>>(stringResponse);

            var createdProject = projects.FirstOrDefault(p => p.Name == project.Name);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotNull(createdProject);
            Assert.Equal(project.Description, createdProject.Description);
            Assert.Equal(project.AuthorId, createdProject.AuthorId);
            Assert.Equal(project.DeadLine, createdProject.DeadLine);
        }



        public void Dispose() {
            _httpClient?.Dispose();
        }
    }
}