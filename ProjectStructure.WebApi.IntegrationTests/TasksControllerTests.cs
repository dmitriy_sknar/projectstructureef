﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using Xunit;

namespace ProjectStructure.WebApi.IntegrationTests {
    public class TasksControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable {
        private readonly HttpClient _httpClient;
        private const string TASKS_URI = "/api/tasks";

        public TasksControllerTests(CustomWebApplicationFactory<Startup> factory) {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteTaskTest() {
            int taskId = 3;
            var postHttpResponse = await _httpClient.DeleteAsync(TASKS_URI + $"/{taskId}");
            postHttpResponse.EnsureSuccessStatusCode();

            var httpResponse = await _httpClient.GetAsync(TASKS_URI);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            List<BsaTaskDto> bsaTaskDtos = JsonConvert.DeserializeObject<List<BsaTaskDto>>(stringResponse);
            var deletedTask = bsaTaskDtos.FirstOrDefault(p => p.Id == taskId);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Null(deletedTask);
        }

        [Theory]
        [InlineData(7, 8)]
        [InlineData(40, 1)]
        [InlineData(500, 0)] //no such user
        [InlineData(-1, 0)] //no such user 
        public async Task GetTasksInProgressForUserTest(int userId, int expectedTasksCount) {
            var httpResponse = await _httpClient.GetAsync(
                TASKS_URI + $"/TasksInProgressForUser/{userId}");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            List<BsaTaskDto> bsaTaskDtos = JsonConvert.DeserializeObject<List<BsaTaskDto>>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(expectedTasksCount, bsaTaskDtos.Count);
            Assert.All(bsaTaskDtos, el => Assert.False((int)TaskStatesEnum.Finished == el.PerformerId));
            Assert.All(bsaTaskDtos, el => Assert.False((int)TaskStatesEnum.Canceled == el.PerformerId));
        }

        public void Dispose() {
            _httpClient?.Dispose();
        }
    }
}