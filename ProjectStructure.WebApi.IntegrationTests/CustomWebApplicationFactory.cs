﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.BL.Tests;
using ProjectStructure.BL.Tests.Files;
using ProjectStructure.Data;
using ProjectStructure.Ef;
using ProjectStructure.Model;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace ProjectStructure.WebApi.IntegrationTests {
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup> {
        protected override void ConfigureWebHost(IWebHostBuilder builder) {
            builder.ConfigureServices(services => {
                
                services.AddControllers();
                
                services.AddSingleton<IDbInitializer, DbInitializer>();
                services.AddSingleton<IRepository<BsaTask>, FakeTasksRepository>();
                services.AddSingleton<IRepository<Team>, FakeTeamsRepository>();
                services.AddSingleton<IRepository<Project>, FakeProjectsRepository>();
                services.AddSingleton<IRepository<User>, FakeUsersRepository>();

                services.AddSingleton<IProjectsService, ProjectsService>();
                services.AddSingleton<IUsersService, UsersService>();
                services.AddSingleton<ITasksService, TasksService>();
                services.AddSingleton<ITeamsService, TeamsService>();
                
            });

            base.ConfigureWebHost(builder);
        }
    }
}