﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;
using Xunit;

namespace ProjectStructure.WebApi.IntegrationTests {
    public class UsersControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable {
        private readonly HttpClient _httpClient;
        private const string USERS_URI = "/api/users";

        public UsersControllerTests(CustomWebApplicationFactory<Startup> factory) {
            _httpClient = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteUserTest() {
            int userId = 1;
            var postHttpResponse = await _httpClient.DeleteAsync(USERS_URI + $"/{userId}");
            postHttpResponse.EnsureSuccessStatusCode();

            var httpResponse = await _httpClient.GetAsync(USERS_URI);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            List<UserDto> users = JsonConvert.DeserializeObject<List<UserDto>>(stringResponse);
            var deletedUser = users.FirstOrDefault(p => p.Id == userId);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Null(deletedUser);
        }

        public void Dispose() {
            _httpClient?.Dispose();
        }
    }
}