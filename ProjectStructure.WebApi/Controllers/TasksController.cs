﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase {

        public IRepository<BsaTask> _repo;
        public ITasksService _tasksService;
        private IMapper _mapper;

        public TasksController(ITasksService tasksService, IRepository<BsaTask> repo, IMapper mapper) {
            _tasksService = tasksService;
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/<BsaTasksController>
        [HttpGet]
        public async Task<IEnumerable<BsaTask>> Get() {
            return await _repo.Get();
        }

        // GET api/<BsaTasksController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BsaTaskDto>> Get(int id) {
            var entityInDb = await _tasksService.Get(id);
            if (entityInDb.Any()) {
                var dto = _mapper.Map<BsaTaskDto>(entityInDb.First());
                return Ok(dto);
            }

            return NotFound();
        }

        // POST api/<BsaTasksController>
        [HttpPost]
        public async Task Post([FromBody] BsaTaskDto dto) {
            await _tasksService.Create(_mapper.Map<BsaTask>(dto));
        }

        // PUT api/<BsaTasksController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] BsaTaskDto dto) {
            var entityInDb = (await _tasksService.Get(id)).FirstOrDefault();
            if (entityInDb != null) {
                // var entity = _mapper.Map<BsaTask>(dto);
                _mapper.Map(dto, entityInDb);
                await _tasksService.Update(entityInDb);
            }
        }

        // DELETE api/<BsaTasksController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id) {
            await _tasksService.Delete(new BsaTask() {
                Id = id
            });
        }

        // GET api/<BsaTasksController>/TasksInProgressForUser/5
        [HttpGet("TasksInProgressForUser/{id}")]
        public async Task<ActionResult<BsaTaskDto>> GetTasksInProgressForUser(int id) {
            var tasksInProgressForUser = await _tasksService.GetTasksInProgressForUser(id);
            var dtos = _mapper.Map<List<BsaTaskDto>>(tasksInProgressForUser);
                return Ok(dtos);
        }
    }
}