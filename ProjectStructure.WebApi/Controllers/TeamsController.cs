﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase {
        public IRepository<Team> _repo;
        public ITeamsService _teamsService;
        private IMapper _mapper;

        public TeamsController(ITeamsService teamsService, IRepository<Team> repo, IMapper mapper) {
            _teamsService = teamsService;
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/<TeamsController>
        [HttpGet]
        public async Task<IEnumerable<Team>> Get() {
            return await _repo.Get();
        }

        // GET api/<TeamsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDto>> Get(int id) {
            var entityInDb = await _teamsService.Get(id);
            if (entityInDb.Any()) {
                var dto = _mapper.Map<TeamDto>(entityInDb.First());
                return Ok(dto);
            }

            return NotFound();
        }

        // POST api/<TeamsController>
        [HttpPost]
        public async Task Post([FromBody] TeamDto dto) {
            await _teamsService.Create(_mapper.Map<Team>(dto));
        }

        // PUT api/<TeamsController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] TeamDto dto) {
            var entityInDb = await _teamsService.Get(id);
            if (entityInDb.Any()) {
                var entity = _mapper.Map<Team>(dto);
                await _teamsService.Update(entity);
            }
        }

        // DELETE api/<TeamsController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id) {
            await _teamsService.Delete(new Team() {
                Id = id
            });
        }
    }
}