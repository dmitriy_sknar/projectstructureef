﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase {

        public IRepository<User> _repo;
        public IUsersService _usersService;
        private IMapper _mapper;

        public UsersController(IUsersService usersService, IRepository<User> repo, IMapper mapper) {
            _usersService = usersService;
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public async Task<IEnumerable<User>> Get() {
            return await _repo.Get();
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> Get(int id) {
            var entityInDb = await _usersService.Get(id);
            if (entityInDb.Any()) {
                var dto = _mapper.Map<UserDto>(entityInDb.First());
                return Ok(dto);
            }

            return NotFound();
        }

        // POST api/<UsersController>
        [HttpPost]
        public async Task Post([FromBody] UserDto dto) {
            await _usersService.Create(_mapper.Map<User>(dto));
        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] UserDto dto) {
            var entityInDb = await _usersService.Get(id);
            if (entityInDb.Any()) {
                var entity = _mapper.Map<User>(dto);
                await _usersService.Update(entity);
            }
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id) {
            await _usersService.Delete(new User() {
                Id = id
            });
        }
    }
}