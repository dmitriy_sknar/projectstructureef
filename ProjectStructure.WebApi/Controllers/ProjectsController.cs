﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Ef;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase {
        public IRepository<Project> _repo;
        public IProjectsService _projectsService;
        private IMapper _mapper;

        public ProjectsController(IProjectsService projectsService, IRepository<Project> repo, IMapper mapper) {
            _projectsService = projectsService;
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        public async Task<IEnumerable<ProjectDto>> Get() {
            IEnumerable<Project> list = await _projectsService.Get();
            var dtoList = _mapper.Map<IEnumerable<ProjectDto>>(list);
            return dtoList;
        }

        // GET api/<ProjectsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> Get(int id) {
            var projectInDb = await _projectsService.Get(id);
            if (projectInDb.Any()) {
                var dto = _mapper.Map<ProjectDto>(projectInDb.First());
                return Ok(dto);
            }

            return NotFound();
        }

        // POST api/<ProjectsController>
        [HttpPost]
        public async Task Post([FromBody] ProjectDto dto) {
            var project = _mapper.Map<Project>(dto);
            await _projectsService.Create(project);
        }

        // PUT api/<ProjectsController>/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] ProjectDto dto) {
            var projectInDb = await _projectsService.Get(id);
            if (projectInDb.Any()) {
                var project = _mapper.Map<Project>(dto);
                await _projectsService.Update(project);
            }
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id) {
            await _projectsService.Delete(new Project() {
                Id = id
            });
        }

        //1. Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        // GET api/<ProjectsController>/GetTasksPerUserProject/5
        [HttpGet("GetTasksPerUserProject/{userId}")]
        public async Task<IEnumerable<TasksPerUserProjectDto>> TasksPerUserProject(int userId) {
            Dictionary<Project, int> dict = await _projectsService.GetTasksPerUserProject(userId);
            return dict.Select(kvp => new TasksPerUserProjectDto {
                ProjectDto = _mapper.Map<ProjectDto>(kvp.Key),
                TasksCount = kvp.Value
            });
        }

        // 2. Получить список тасков, назначенных на конкретного пользователя(по id), где name таска< 45 символов (коллекция из тасков).
        // GET api/<ProjectsController>/GetTasksPerUserWithNameLengthUpTo45Symbols/5
        [HttpGet("GetTasksPerUserWithNameLengthUpTo45Symbols/{userId}")]
        public async Task<IEnumerable<BsaTaskDto>> TasksPerUserWithNameLengthUpTo45Symbols(int userId) {
            var tasks = await _projectsService.GetTasksPerUserWithNameLengthUpTo45Symbols(userId);
            return _mapper.Map<IEnumerable<BsaTaskDto>>(tasks);
        }

        // 3. Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2020) году для конкретного пользователя(по id).
        // GET api/<ProjectsController>/TasksPerUserFinishedIn2020/5
        [HttpGet("TasksPerUserFinishedIn2020/{userId}")]
        public async Task<IEnumerable<TasksPerUserFinishedIn2020Dto>> TasksPerUserFinishedIn2020(int userId) {
            var tasks = await _projectsService.GetTasksPerUserFinishedIn2020(userId);
            return tasks.Select(task => new TasksPerUserFinishedIn2020Dto(task.Id, task.Name));
        }

        // 4. Получить список(id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет,
        // отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        //     P.S - в этом запросе допускается проверить только год рождения пользователя, без привязки к месяцу/дню/времени рождения.
        // GET api/<ProjectsController>/CommandsWithUsersOlder10Years
        [HttpGet("CommandsWithUsersOlder10Years")]
        public async Task<IEnumerable<CommandsWithUsersOlder10YearsDto>> GetCommandsWithUsersOlder10Years() {
            var teams = await _projectsService.GetTeamsWhereAllUsersOlder10Years();

            return teams.Select(t => new CommandsWithUsersOlder10YearsDto(
                    t.Id,
                    t.Name,
                    _mapper.Map<IEnumerable<UserDto>>(
                        t.TeamMates.OrderByDescending(u => u.RegisteredAt)) //по дате регистрации пользователя по убыванию - did you mean this? 
                )
            );
        }

        // 5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        // GET api/<ProjectsController>/UsersSortedByFirstNameAndTaskNameLength
        [HttpGet("UsersSortedByFirstNameAndTaskNameLength")]
        public async Task<IEnumerable<UserDto>> GetUsersSortedByFirstNameAndTaskNameLength() {
            var users = await _projectsService.GetUsersSortedByFirstNameAndTaskNameLength();
            return _mapper.Map<IEnumerable<UserDto>>(users);
        }

        // 6.Получить следующую структуру (передать Id пользователя в параметры):
        // User
        // Последний проект пользователя(по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)
        // P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        // GET api/<ProjectsController>/UserAndProjectsDetails/5
        [HttpGet("UserAndProjectsDetails/{userId}")]
        public async Task<UserAndProjectsDetailsDto> GetUserAndProjectsDetails(int userId) {
            var details = await _projectsService.GetUserAndProjectsDetailsSortedByProjectCreatedDateDescending(userId);

            var detailsDtos = details
                .Select(proj => new UserAndProjectsDetailsDto(
                    _mapper.Map<UserDto>(proj.Author),
                    _mapper.Map<ProjectDto>(proj),
                    proj.Tasks.Count,
                    proj.Tasks
                        .Where(t => t.FinishedAt == null)
                        .Select(t => t)
                        .Count(),
                    _mapper.Map<BsaTaskDto>(proj.Tasks
                        .OrderByDescending(t => (t.FinishedAt.Subtract(t.CreatedAt)))
                        .FirstOrDefault())
                )).FirstOrDefault();
            return detailsDtos;
        }

        //7. Получить следующую структуру:
        // Проект
        // Самый длинный таск проекта(по описанию)
        // Самый короткий таск проекта(по имени)
        // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков< 3
        // GET api/<ProjectsController>/ProjectWithDetails
        [HttpGet("ProjectWithDetails")]
        public async Task<IEnumerable<ProjectWithDetailsDto>> GetProjectWithDetails() {
            var projectsWithDetaisDict = await _projectsService.GetProjectWithNameLengthFrom20AndTasksCountTo3WithTeamMatesCount();
            var projectWithDetails = projectsWithDetaisDict
                .Select(kvp => {

                    return new ProjectWithDetailsDto(
                        _mapper.Map<ProjectDto>(kvp.Key),
                        _mapper.Map<BsaTaskDto>(kvp.Key.Tasks
                            .OrderByDescending(t => t.Description.Length)
                            .FirstOrDefault()),
                        _mapper.Map<BsaTaskDto>(kvp.Key.Tasks
                            .OrderBy(t => t.Name.Length)
                            .FirstOrDefault()),
                        kvp.Value
                    );
                });
            return projectWithDetails;
        }
    }
}