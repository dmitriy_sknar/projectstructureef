using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.BL.Services;
using ProjectStructure.Data;
using ProjectStructure.Ef;
using ProjectStructure.Model;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace ProjectStructure.WebApi {
    public class Startup {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();

            services.AddDbContext<ProjectStructureDbContext>(
                options => {
                    options.UseSqlServer(Configuration.GetConnectionString("ProjectStructureDBConnection"));
                    options.EnableSensitiveDataLogging();
                },
                ServiceLifetime.Transient
                );

            services.AddScoped<IDbInitializer, DbInitializer>();
            services.AddScoped<IRepository<BsaTask>, BaseRepository<BsaTask>>();
            services.AddScoped<IRepository<Team>, BaseRepository<Team>>();
            services.AddScoped<IRepository<Project>, BaseRepository<Project>>();
            services.AddScoped<IRepository<User>, BaseRepository<User>>();
            services.AddScoped<IRepository<TaskState>, BaseRepository<TaskState>>();

            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<ITeamsService, TeamsService>();

            MapperConfiguration mappingConf = new MapperConfiguration(mc => { mc.AddProfile(typeof(MapConfig)); });
            IMapper mapper = mappingConf.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}